//Variables
const btnSend = document.querySelector('#send');
const form  = document.querySelector('#send-mail');
const resetBtn = document.querySelector('#resetBtn');

//Field variables
const email = document.querySelector('#email');
const subject = document.querySelector('#subject');
const message = document.querySelector('#message');

eventListener();
function eventListener() {
    //app start
    document.addEventListener('DOMContentLoaded', appInit);

    //form field
    email.addEventListener('blur', validateForm);
    subject.addEventListener('blur', validateForm);
    message.addEventListener('blur', validateForm);

    //send form
    form.addEventListener('submit', sendEmail);

    //reset form
    resetBtn.addEventListener('click', resetForm);
}

//Functions
function appInit() {
    
}

function validateForm(e) {
    let field = e.target;

    //validate empty field
    if(field.value.length === 0) {
        showError(field, 'Todos los campos son obligadorios');
        disabledBtnSend();
    } else {
        //validate email
        if( field.type === 'email' ) {
            if( ! isEmail(field)) {
                showError(field, 'El correo no es válido.');
                disabledBtnSend();
            } else {
                removeMessageError();
                showPass(field);
            }
        } else {
            removeMessageError();
            showPass(field);
        }
    } 

    if( validationPass() ) {
        enabledBtnSend();
    }
}

function showMessageError(msg) {
    const p = document.createElement('p');
    p.textContent = msg;
    p.classList.add('error', 'border', 'border-red-500', 'background-red-100', 'text-red-500', 'p-3', 'mt-5', 'text-center');
   
    const errors = document.querySelectorAll('.error');
    if(errors.length === 0 ) {
        form.insertBefore(p, document.querySelector('.mb-10'));

        //add at the botton
        //form.appendChild(p);
    } else {
        errors[0].remove();
        form.insertBefore(p, document.querySelector('.mb-10'));
    }
    
}

function enabledBtnSend() {
    btnSend.disabled = false;
    btnSend.classList.remove('cursor-not-allowed','opacity-50');
}

function disabledBtnSend() {
    btnSend.disabled = true;
    btnSend.classList.add('cursor-not-allowed','opacity-50');
}

function removeMessageError() {
    if( document.querySelector('.error') ) {
        document.querySelector('.error').remove();
    }
}

function isEmpty(field) {
    return field.value === '';
}

function validationPass() {
    let valid = false;

     if ( ! isEmpty(email) && ! isEmpty(subject) && ! isEmpty(message) ) {
         if( isEmail(email) ) {
             valid = true;
         }
    }

    return valid;
}

function showError(field, msg) {
    field.classList.remove('border', 'border-green-500');
    field.classList.add('border', 'border-red-500');
    showMessageError(msg);
}

function showPass(field) {
    field.classList.remove('border', 'border-red-500');
    field.classList.add('border', 'border-green-500');
}

function isEmail(field) {
    const expReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return expReg.test(field.value);
}

function sendEmail(e) {
    e.preventDefault();
    
    //show spiner
    const spinner = document.querySelector('#spinner');
    spinner.style.display = 'flex';

    setTimeout( () => {
        spinner.style.display = 'none';

        const p = document.createElement('p');
        p.textContent = 'Mensaje enviado correctamente.';
        p.classList.add('text-center', 'my-10', 'p-2', 'bg-green-500', 'text-white', 'font-bold', 'uppercase');
        form.insertBefore(p, spinner);

        setTimeout( () => { 
            p.remove();
            resetForm();
        }, 5000 );

    }, 3000 );
}

function resetForm() {
    form.reset();
}
